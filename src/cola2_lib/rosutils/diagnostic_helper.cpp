
/*
 * Copyright (c) 2018 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */


#include "cola2_lib/rosutils/diagnostic_helper.h"

namespace cola2 {
namespace rosutils {

DiagnosticHelper::DiagnosticHelper(ros::NodeHandle& n, const std::string name, const std::string hardware_id):
  counter_(0),
  current_freq_(0.0),
  times_not_ok_(0)
{
  diagnostic_pub_ = n.advertise<diagnostic_msgs::DiagnosticArray>(cola2::rosutils::getNamespace() + "/diagnostics", 1);
  diagnostic_.name = name;
  diagnostic_.hardware_id = hardware_id;
  last_check_freq_ = ros::Time().now().toSec();
}

void DiagnosticHelper::setLevel(const int level, const std::string message)
{
  diagnostic_.level = level;
  if (message.compare("none") == 0)
  {
    if (level == diagnostic_msgs::DiagnosticStatus::OK)
      diagnostic_.message = "Ok";
    else if (level == diagnostic_msgs::DiagnosticStatus::WARN)
      diagnostic_.message = "Warning";
    else
      diagnostic_.message = "Error";
  }
  else
  {
    diagnostic_.message = message;
  }
  // Publish diagnostic message
  publish();
}

void DiagnosticHelper::add(const std::string key, const bool value)
{
  // change value to string
  if (value)
    add(key, std::string("True"));
  else
    add(key, std::string("False"));
}

void DiagnosticHelper::add(const std::string key, const int value)
{
  // change value to string
  std::stringstream ss;
  ss << value;
  add(key, ss.str());
}

void DiagnosticHelper::add(const std::string key, const double value)
{
  std::stringstream ss;
  ss << value;
  // change value to string
  add(key, ss.str());
}

void DiagnosticHelper::add(const std::string key, const std::string value)
{
  // Create KeyValue type
  diagnostic_msgs::KeyValue key_value;
  key_value.key = key;
  key_value.value = value;

  // Search for key
  std::vector<diagnostic_msgs::KeyValue>::iterator it = diagnostic_.values.begin();
  bool found = false;
  while (it != diagnostic_.values.end() && !found)
  {
    if (it->key.compare(key) == 0)
      found = true;
    else
      it++;
  }

  // If key already found in 'values' delete it
  if (it != diagnostic_.values.end())
  {
    diagnostic_.values.erase(it);
  }

  // Add Key Value
  diagnostic_.values.push_back(key_value);
}

void DiagnosticHelper::del(const std::string key)
{
  // Search for key
  std::vector<diagnostic_msgs::KeyValue>::iterator it = diagnostic_.values.begin();
  bool found = false;
  while (it != diagnostic_.values.end() && !found)
  {
    if (it->key.compare(key) == 0)
      found = true;
    it++;
  }

  // If key already found in 'values' delete it
  if (it != diagnostic_.values.end())
  {
    diagnostic_.values.erase(it);
  }
}

void DiagnosticHelper::publish()
{
  if (counter_ > 0) // compute frequency
  {
    // ensure that a minimum of seconds have passed to compute frequency
    if (ros::Time().now().toSec() - last_check_freq_ > 3.0)
    {
      current_freq_ =  counter_ / (ros::Time().now().toSec() - last_check_freq_);
      add("frequency", current_freq_);
      counter_ = 0;
      last_check_freq_ = ros::Time().now().toSec();
    }
  }

  // Published as a warning, error or stale
  if (diagnostic_.level != diagnostic_msgs::DiagnosticStatus::OK)
  {
    times_not_ok_++;
  }
  else
  {
    times_not_ok_ = 0;
  }

  // Publish diagnostic
  diagnostic_msgs::DiagnosticArray diagnostic_array;
  diagnostic_array.header.stamp = ros::Time::now();
  diagnostic_array.header.frame_id = diagnostic_.name;
  diagnostic_array.status.push_back(diagnostic_);
  diagnostic_pub_.publish(diagnostic_array);
}

void DiagnosticHelper::increaseFrequencyCounter()
{
  counter_++;
}

double DiagnosticHelper::getCurrentFreq()
{
  return current_freq_;
}

unsigned int DiagnosticHelper::getTimesNotOK()
{
  return times_not_ok_;
}

}  // namespace rosutils
}  // namespace cola2
