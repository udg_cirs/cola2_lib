
/*
 * Copyright (c) 2018 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#include "cola2_lib/utils/angles.h"

double cola2::utils::wrapAngle(const double angle)
{
  return (angle + (2.0 * M_PI * floor((M_PI - angle) / (2.0 * M_PI))));
}

//*****************************************************************************
// Angle unit conversions
//*****************************************************************************

double cola2::utils::degreesToRadians(const double degrees)
{
  return (degrees / 180.0) * M_PI;
}

double cola2::utils::radiansToDegrees(const double radians)
{
  return (radians / M_PI) * 180.0;
}

double cola2::utils::gradiansToRadians(const double value)
{
  return value * M_PI / 200.0;
}

double cola2::utils::radiansToGradians(const double value)
{
  return value * 200.0 / M_PI;
}

double cola2::utils::degreeMinutesToDegrees(const double degree_minutes, const char hemisphere)
{
  assert(hemisphere == 'N' || hemisphere == 'S' || hemisphere == 'E' || hemisphere == 'W');

  unsigned int degrees = static_cast<unsigned int>(degree_minutes / 100);
  double minutes = degree_minutes - (degrees * 100);

  if (hemisphere == 'E' || hemisphere == 'N')
  {
    return degrees + (minutes / 60.0);
  }
  else if (hemisphere == 'W' || hemisphere == 'S')
  {
    return -(degrees + (minutes / 60.0));
  }
  return 0.0;
}

double cola2::utils::degreeMinutesToDegreesInt(const double degree_minutes, const int hemisphere)
{
  switch (hemisphere)
  {
    case 0:
      return degreeMinutesToDegrees(degree_minutes, static_cast<char>('N'));
    case 1:
      return degreeMinutesToDegrees(degree_minutes, static_cast<char>('S'));
    case 2:
      return degreeMinutesToDegrees(degree_minutes, static_cast<char>('W'));
    case 3:
      return degreeMinutesToDegrees(degree_minutes, static_cast<char>('E'));
    default:
      // std::cerr << "Invalid hemisphere: " << hemisphere << "\n";
      return 0.0;
  }
}

//*****************************************************************************
// Quaternion-Euler conversions
//*****************************************************************************

// From the Matrix 3x3 Class of tf package
Eigen::Vector3d cola2::utils::rotation2euler(const Eigen::Matrix3d& rotation, unsigned int solution_number)
{
  Eigen::Vector3d rpy;

  struct Euler
  {
    double yaw;
    double pitch;
    double roll;
  };

  Euler euler_out;
  Euler euler_out2;  // second solution

  // Check that pitch is not at a singularity
  if (fabs(rotation(2, 0)) >= 1)
  {
    euler_out.yaw = 0;
    euler_out2.yaw = 0;

    // From difference of angles formula
    if (rotation(2, 0) < 0)  // gimbal locked down
    {
      double delta = atan2(rotation(0, 1), rotation(0, 2));
      euler_out.pitch = M_PI / 2.0;
      euler_out2.pitch = M_PI / 2.0;
      euler_out.roll = delta;
      euler_out2.roll = delta;
    }
    else  // gimbal locked up
    {
      double delta = atan2(-rotation(0, 1), -rotation(0, 2));
      euler_out.pitch = -M_PI / 2.0;
      euler_out2.pitch = -M_PI / 2.0;
      euler_out.roll = delta;
      euler_out2.roll = delta;
    }
  }
  else
  {
    euler_out.pitch = -asin(rotation(2, 0));
    euler_out2.pitch = M_PI - euler_out.pitch;

    euler_out.roll = atan2(rotation(2, 1) / cos(euler_out.pitch), rotation(2, 2) / cos(euler_out.pitch));
    euler_out2.roll = atan2(rotation(2, 1) / cos(euler_out2.pitch), rotation(2, 2) / cos(euler_out2.pitch));

    euler_out.yaw = atan2(rotation(1, 0) / cos(euler_out.pitch), rotation(0, 0) / cos(euler_out.pitch));
    euler_out2.yaw = atan2(rotation(1, 0) / cos(euler_out2.pitch), rotation(0, 0) / cos(euler_out2.pitch));
  }
  if (solution_number == 1)
  {
    rpy(0) = euler_out.roll;
    rpy(1) = euler_out.pitch;
    rpy(2) = euler_out.yaw;
  }
  else
  {
    rpy(0) = euler_out2.roll;
    rpy(1) = euler_out2.pitch;
    rpy(2) = euler_out2.yaw;
  }

  return rpy;
}

Eigen::Matrix3d cola2::utils::euler2rotation(const Eigen::Vector3d &rpy)
{
  return Eigen::Matrix3d(cola2::utils::euler2quaternion(rpy));
}

Eigen::Vector3d cola2::utils::quaternion2euler(const Eigen::Quaterniond& quat, unsigned int solution_number)
{
  return cola2::utils::rotation2euler(quat.toRotationMatrix(), solution_number);
}

Eigen::Quaterniond cola2::utils::euler2quaternion(const double roll, const double pitch, const double yaw)
{
  Eigen::AngleAxisd rollAngle(roll, Eigen::Vector3d::UnitX());
  Eigen::AngleAxisd pitchAngle(pitch, Eigen::Vector3d::UnitY());
  Eigen::AngleAxisd yawAngle(yaw, Eigen::Vector3d::UnitZ());

  Eigen::Quaterniond q = yawAngle * pitchAngle * rollAngle;
  return q;
}

Eigen::Quaterniond cola2::utils::euler2quaternion(const Eigen::Vector3d& rpy)
{
  return cola2::utils::euler2quaternion(rpy(0), rpy(1), rpy(2));
}
