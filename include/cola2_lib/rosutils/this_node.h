#ifndef THIS_NODE_H
#define THIS_NODE_H

#include <string>

namespace cola2
{
namespace rosutils
{
/**
 * Returns the namespace of the node without double slashes.
 */
std::string getNamespace();

/**
 * Returns the namespace of the node without double slashes and without the leading dash (useful for frames).
 */
std::string getNamespaceNoInitialDash();

/**
 * Returns the unresolved name of the node (without the namespace).
 * if the node name is /swarm1/girona500/node returns node
 */
std::string getUnresolvedNodeName();

}  // namespace rosutils
}  // namespace cola2

#endif  // THIS_NODE_H
