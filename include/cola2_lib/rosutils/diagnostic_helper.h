
/*
 * Copyright (c) 2018 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_ROSUTILS_DIAGNOSTIC_HELPER_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_ROSUTILS_DIAGNOSTIC_HELPER_H_

#include <ros/ros.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>
#include <string>
#include <vector>
#include <cola2_lib/rosutils/this_node.h>

namespace cola2
{
namespace rosutils
{
/**
 * \brief Helper class to work with ROS diagnostics. Manages the publishing of the diagnostics,
 * the addition/supression of diagnostic entries and checks its publishing frequency.
 */
class DiagnosticHelper
{

private:
  // Diagnostic msg
  diagnostic_msgs::DiagnosticStatus diagnostic_;

  // ROS Publisher
  ros::Publisher diagnostic_pub_;

  // Variables to check frequency
  unsigned int counter_;
  double last_check_freq_;
  double current_freq_;

  // To check the times that the diagnostics are in a state different from OK (warn, error, stale)
  unsigned int times_not_ok_;

public:

    /**
     * For the node n, sets the diagnostics publisher and configures the name and hardware_id
     */
  DiagnosticHelper(ros::NodeHandle& n, const std::string name, const std::string hardware_id);

    /**
     * Sets the level and the message of the Diagnostics message
     */
  void setLevel(const int level, const std::string message = "none");

    /**
     * Add a diagnostics entry with a bool value
     */
  void add(const std::string key, const bool value);
    /**
     * Add a diagnostics entry with an integer value
     */
  void add(const std::string key, const int value);
    /**
     * Add a diagnostics entry with a double value
     */
  void add(const std::string key, const double value);
    /**
     * Add a diagnostics entry with a string value
     */
  void add(const std::string key, const std::string value);

    /**
     * Delete a diagnostics entry
     */
  void del(const std::string key);

    /**
     * Publish the diagnostics message
     */
  void publish();

    /**
     * Increments the counter that is used to compute the frequency in which this method is called.
     * (to be called externally from other nodes)
     */
  void increaseFrequencyCounter();

    /**
     * Gets current diagnostics frequency
     */
  double getCurrentFreq();

    /**
     * Returns counter with the number of times the diagnostic message was in a state other than OK
     */
  unsigned int getTimesNotOK();

};

}  // namespace rosutils
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_ROSUTILS_DIAGNOSTIC_HELPER_H_
