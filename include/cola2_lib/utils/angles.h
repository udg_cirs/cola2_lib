
/*
 * Copyright (c) 2018 Iqua Robotics SL - All Rights Reserved
 *
 * This file is subject to the terms and conditions defined in file
 * 'LICENSE.txt', which is part of this source code package.
 */

#ifndef COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_UNITS_UTIL_H_
#define COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_UNITS_UTIL_H_

#include <math.h>
#include <iostream>
#include <eigen3/Eigen/Geometry>

namespace cola2
{
namespace utils
{
/**
 * Wraps an angle (in radians) between [-PI,PI]
 */
double wrapAngle(const double angle);

//*****************************************************************************
// Angle unit conversions
//*****************************************************************************

/**
 * Converts angle from degrees to radians
 */
double degreesToRadians(const double value);

/**
  * Converts angle from radians to degrees
  */
double radiansToDegrees(const double value);

/**
  * Converts angle from gradians to radians.
  * A gradian is equivalent to 1/400 of a turn, 9/10 of a degree, or PI/200 of a radian.
  */
double gradiansToRadians(const double value);

/**
  * Converts angle from radians to gradians.
  * A gradian is equivalent to 1/400 of a turn, 9/10 of a degree, or PI/200 of a radian.
 */
double radiansToGradians(const double value);

/**
  * Converts degree_minutes (DDMM.MM) to decimal degrees (DD.DD)
  * taking into account the hemisphere passed as a character.
  */
double degreeMinutesToDegrees(const double degree_minutes, const char hemisphere);

/**
 * Converts degree_minutes (DDMM.MM) to decimal degrees (DD.DD)
 * taking into account the hemisphere passed as an integer.
 */
double degreeMinutesToDegreesInt(const double degree_minutes, const int hemisphere);

//*****************************************************************************
// Quaternion-Euler conversions
//*****************************************************************************

/**
 * Get vector of euler angles (roll, pitch, yaw) from rotation matrix
 */
Eigen::Vector3d rotation2euler(const Eigen::Matrix3d& rotation, unsigned int solution_number = 1);

/**
 * Get rotation matrix from vector of euler angles (roll, pitch, yaw)
 */
Eigen::Matrix3d euler2rotation(const Eigen::Vector3d &rpy);

/**
 * Get vector of euler angles (roll, pitch, yaw) from quaternion
 */
Eigen::Vector3d quaternion2euler(const Eigen::Quaterniond& quat, unsigned int solution_number = 1);

/**
 * Get quaternion matrix from euler angles (roll, pitch, yaw)
 */
Eigen::Quaterniond euler2quaternion(const double roll, const double pitch, const double yaw);
/**
 * Get quaternion matrix from euler angles (roll, pitch, yaw)
 */
Eigen::Quaterniond euler2quaternion(const Eigen::Vector3d& rpy);

}  // namespace util
}  // namespace cola2

#endif  // COLA2_LIB_INCLUDE_COLA2_LIB_UTILS_UNITS_UTIL_H_
